<?php declare(ticks = 1);
namespace Core;

use Symfony\Component\Process\Process as SymfonyProcess;

abstract class Process extends AbstractCommand implements InterfaceProcess
{
    const STATUS_INSERT = 'insert',
          STATUS_STARTED = 'started',
          STATUS_RUNNING = 'running',
          STATUS_FINISHED = 'finished',
          STATUS_RELOAD = 'reload',
          STATUS_ERROR = 'error';

    private $process;
    private $activeProcess;
    private $typeLoadProcess = 'finished';
    private $checkFrequency = 100; // 100ms

    final protected function init() {}

    public function setProcessWhile()
    {
        $this->typeLoadProcess = true;
    }

    private function getTypeProcess()
    {
        return ($this->typeLoadProcess === 'finished') ? count($this->activeProcess) : true;
    }

    public function setCheckFrequency($microSecond)
    {
        $this->checkFrequency = $microSecond;
    }

    public function addProcess($class, $method, array $arguments = [], array $options = [], $startLimit = 1)
    {
        if (!class_exists($class)) {
            throw new \InvalidArgumentException(sprintf('Not found class name: %s', $class));
        }

        if (!isset($options['timeout'])) {
            $options['timeout'] = 0;
        }

        $command = $this->buildCommand($class, $method, $arguments) . $this->processOptions($options);

        $this->buildProcess($class, $command, $options, $startLimit);
    }

    public function addProcessCommand($name, $command, array $options = [], $startLimit = 1)
    {
        $this->buildProcess($name, $command, $options, $startLimit);
    }

    private function buildProcess($name, $command, array $options = [], $startLimit)
    {
        $interval = isset($options['interval']) ? (int) $this->getTimeOut($options['interval']) : 0;
        $reloadInterval = isset($options['reloadInterval']) ? $options['reloadInterval'] : false;

        $timeout = 0;

        for ($i=0; $i < $startLimit; $i++) {

            if ($reloadInterval !== false) {
                $timeout += $interval;
            }

            $this->process[][$name] = [
                'command' => $command,
                'status' => self::STATUS_INSERT,
                'options' => [
                    'reload' => isset($options['reload']) ? $options['reload'] : false,
                    'interval' => $timeout,
                ]
            ];
        }
    }

    private function buildCommand($class, $method, array $arguments = [])
    {
        $reflection = new \ReflectionClass($class);

        if ($reflection->getParentClass()->name !== Controller::class) {
            throw new \InvalidArgumentException(sprintf('Class name: %s is not a controller', $class));
        }

        $action = $method . Loader::CONTROLLER_PREFIX_ACTION;

        if (!$reflection->hasMethod($action)) {
            throw new \InvalidArgumentException(sprintf('Class not found method action name: %s', $method));
        }

        $methodClass = $reflection->getMethod($action);
        $methodParams = $methodClass->getParameters();
        $args = [];

        foreach ($methodParams as $key => $params) {
            $nameArg = $params->getName();

            if (!isset($arguments[$nameArg]) && $params->isDefaultValueAvailable() !== true) {
                throw new \InvalidArgumentException(sprintf('Not found argument: %s action: %s', $nameArg, $action));
            }

            if ($params->isDefaultValueAvailable() === true && !empty($arguments[$nameArg])) {
                if ($params->getType() == 'array' && !is_array($arguments[$nameArg])) {
                    throw new \InvalidArgumentException(sprintf('Argument: %s action: %s is not array', $nameArg, $action));
                }
            }

            if ($params->getType() == 'array' && !is_array($arguments[$nameArg])) {
                throw new \InvalidArgumentException(sprintf('Argument: %s action: %s is not array', $nameArg, $action));
            }

            if (isset($arguments[$nameArg])) {
                if ($params->getType() == 'array') {
                    $args[$nameArg] = "'" . json_encode($arguments[$nameArg], true) . "'";
                } else {
                    $args[$nameArg] = "'" . $arguments[$nameArg] . "'";
                }
            }
        }

        $nameCommand = $this->getNameCommand($class);

        return $this->getFullCommand($nameCommand, $method, $args);
    }

    private function getFullCommand($nameCommand, $method, $arguments)
    {
        $config = Config::getInstance();

        $command = [
            $config->getConfig('application.php-cli-path'),
            $config->getConsolePath(),
            $nameCommand,
            $method,
            implode(' ', $arguments)
        ];

        return implode(' ', $command);
    }

    private function getNameCommand($class)
    {
        $name = Loader::CONTROLLER_NAMESPACE . ':' . str_replace([Loader::CONTROLLER_NAMESPACE . '\\', Loader::CONTROLLER_PREFIX_NAME], '', $class);

        return strtolower($name);
    }

    private function processOptions(array $options)
    {
        $command = '';

        foreach ($options as $key => $option) {
            if (in_array($key, ['memory', 'timeout'])) {
                $command .= ' --' . $key . '=' . $option . ' ';
            }
        }

        return $command;
    }

    private function startProcess()
    {
        foreach ($this->process as $key => $process) {
            foreach ($process as $keyName => $command) {
                if (in_array($command['status'], [self::STATUS_INSERT, self::STATUS_STARTED, self::STATUS_RELOAD])) {

                    if ($command['status'] === self::STATUS_RELOAD) {
                        if (class_exists($keyName)) {
                            $command['command'] = preg_replace('/--timeout=([0-9:ms|m|s]+) /i', '--timeout=' . $command['options']['interval'], $command['command']);
                        } else {
                            usleep($command['options']['interval']);
                        }
                    }

                    $process = new SymfonyProcess($command['command']);
                    $process->start();
                    $this->activeProcess[$key][$keyName] = $process;
                    $this->process[$key][$keyName]['status'] = self::STATUS_RUNNING;
                }
            }
        }
    }

    final protected function executeResult()
    {
        $this->buildProcessCommand();

        $this->startProcess();

        $this->wait();
    }

    private function wait()
    {
        while ($this->getTypeProcess()) {
            foreach ($this->activeProcess as $key => $process) {
                foreach ($process as $keyName => $runningProcess) {
                    if ($runningProcess instanceof SymfonyProcess) {
                        if (!$runningProcess->isRunning()) {

                            if ($runningProcess->getExitCode() > 0) {

                                if ($this->process[$key][$keyName]['options']['reload'] === true) {
                                    $this->process[$key][$keyName]['status'] = self::STATUS_RELOAD;
                                } else {
                                    $this->process[$key][$keyName]['status'] = self::STATUS_ERROR;
                                }

                            } else {
                                if ($this->process[$key][$keyName]['options']['reload'] === true) {
                                    $this->process[$key][$keyName]['status'] = self::STATUS_RELOAD;
                                } else {
                                    $this->process[$key][$keyName]['status'] = self::STATUS_FINISHED;
                                }
                            }

                            $this->finishedProcess($runningProcess, $this->process[$key][$keyName]['status']);

                            unset($this->activeProcess[$key][$keyName]);
                        }
                    }

                    usleep($this->checkFrequency);
                    $this->startProcess();
                }
            }
        }
    }
}