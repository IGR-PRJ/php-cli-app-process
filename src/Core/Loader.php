<?php
namespace Core;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;

class Loader
{
    const CONTROLLER_NAMESPACE = 'Controller';
    const CONTROLLER_PREFIX_NAME = 'Controller';
    const CONTROLLER_PREFIX_ACTION = 'Action';

    const PROCESS_NAMESPACE = 'Process';
    const PROCESS_PREFIX_NAME = 'Process';


    private $application;
    private $argv;
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->application = new Application($this->config->getConfig('application.name'), $this->config->getConfig('application.version'));
        $this->argv = new ArgvInput();

        $this->loadApplication();
    }

    private function loadApplication()
    {
        $this->application->addCommands($this->getBuildCommandList(self::CONTROLLER_NAMESPACE, self::CONTROLLER_PREFIX_NAME));
        $this->application->addCommands($this->getBuildCommandList(self::PROCESS_NAMESPACE, self::PROCESS_PREFIX_NAME));
    }

    private function getBuildCommandList($namespace, $prefixName)
    {
        $list = [];
        $files = array_slice(scandir($this->config->getSrcDir() . $namespace), 2);

        foreach ($files as $file) {
            $name = str_replace($prefixName . '.php', '', $file);
            $className = sprintf('%s\\%s%s', $namespace, $name, $prefixName);
            $commandName = sprintf('%s:%s', strtolower($namespace), strtolower($name));
            $list[] = new $className($commandName);
        }

        return $list;
    }

    public function runApplication()
    {
        return $this->application->run($this->argv);
    }
}