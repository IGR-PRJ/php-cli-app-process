<?php
namespace Core;

class Config
{
    private static $_instance = null;

    private $config = [
        'application' => [
            'name' => '',
            'version' => '',
            'php-cli' => '',
        ]
    ];

    private $path;

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct(){}
    protected function __clone(){}

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        if (is_null($this->path)) {
            return $this->getRootDir();
        }

        return $this->path;
    }

    public function getRootDir()
    {
        return realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    public function getSrcDir()
    {
        return $this->getRootDir() . 'src' . DIRECTORY_SEPARATOR;
    }

    private function getConfigDir()
    {
        return $this->getSrcDir() . 'Config' . DIRECTORY_SEPARATOR;
    }

    public function getConsolePath()
    {
        return $this->getRootDir() . 'bin' . DIRECTORY_SEPARATOR  . 'console';
    }

    public function loadConfig()
    {
        $path = $this->getConfigDir() . 'application.ini';

        if (!file_exists($path)) {
            throw new \RuntimeException(sprintf('Not found file: %s', $path));
        }

        $this->config = array_replace_recursive($this->config, parse_ini_file($path, true, INI_SCANNER_TYPED));
    }

    public function getConfig($keyNames)
    {
        $arr = $this->config;
        $keys = (strpos($keyNames, '.') !== false)  ? explode('.', $keyNames) : [$keyNames];

        foreach ($keys as $key) {
            if (isset($arr[$key])) {
                $arr = $arr[$key];
            }
        }

        return $arr;
    }
}