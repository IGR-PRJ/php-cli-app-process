<?php
namespace Core;

use Symfony\Component\Console\Input\InputArgument;

class Controller extends AbstractCommand
{
    final protected function init()
    {
        $this->addArgument('actionName', InputArgument::REQUIRED, 'Controller action name.');
        $this->addArgument('arguments', InputArgument::IS_ARRAY, 'Arguments controller.');
    }

    private function getJson($string, $argName)
    {
        $json = json_decode($string, true);

        if (!(json_last_error() == JSON_ERROR_NONE && is_array($json))) {
            throw new \InvalidArgumentException(sprintf('Argument name: %s is not valid json', $argName));
        }

        return $json;
    }

    private static function getCalled()
    {
        return get_called_class();
    }

    final protected function executeResult()
    {
        $action = $this->getInput()->getArgument('actionName') . Loader::CONTROLLER_PREFIX_ACTION;
        $arguments = $this->getInput()->getArgument('arguments');

        if (!method_exists(self::getCalled(), $action)) {
            throw new \Exception(sprintf('Not found action: %s', $action));
        }

        $methodClass = new \ReflectionMethod($this, $action);
        $methodParams = $methodClass->getParameters();
        $args = [];

        foreach ($methodParams as $key => $params) {
            $nameArg = $params->getName();

            if (!isset($arguments[$key]) && $params->isDefaultValueAvailable() !== true) {
                throw new \InvalidArgumentException(sprintf('Not found argument: %s action: %s', $nameArg, $action));
            }

            if (isset($arguments[$key])) {
                if ($params->getType() == 'array') {
                    $args[$nameArg] = $this->getJson($arguments[$key], $nameArg);
                } else {
                    $args[$nameArg] = $arguments[$key];
                }
            }
        }

        return $result = $methodClass->invokeArgs($this, $args);
    }
}