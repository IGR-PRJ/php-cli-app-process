<?php
namespace Core;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractCommand extends Command
{
    private $input;
    private $output;
    private $io;
    private $sleep;

    final public function __construct($name = null)
    {
        parent::__construct($name);

        pcntl_signal(SIGTERM, [&$this, 'stop']); // kill
        pcntl_signal(SIGINT, [&$this, 'stop']); // ctrl+c

        $this->addOption('timeout', null, InputOption::VALUE_OPTIONAL, 'Sleep timeout. Format "--timeout=number:format". Formats ms, s, m. Default format ms.');
        $this->addOption('memory', null, InputOption::VALUE_OPTIONAL, 'Memory limit script.');

        $this->init();
    }

    abstract protected function init();
    abstract protected function executeResult();

    public function getDescription()
    {
        return get_called_class();
    }

    final protected function stop($signo)
    {
        exit(0);
    }

    protected function getTime()
    {
        return date('d-m-Y H:i:s T', time());
    }

    /**
     * @return InputInterface
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @return SymfonyStyle
     */
    public function getIO()
    {
        return $this->io;
    }

    private function sleep()
    {
        if (!is_null($this->sleep)) {
            if (strpos($this->sleep, ':') !== false) {
                $args = explode(':', $this->sleep);

                if (is_numeric($args[0])) {
                    switch ($args[1]) {
                        case 'ms' : usleep( (int) $args[0]);
                            break;
                        case 's' : sleep( (int) $args[0]);
                            break;
                        case 'm' : sleep( (int) $args[0] * 60);
                            break;
                    }
                }

            } else {
                if (is_numeric($this->sleep)) {
                    usleep( (int) $this->sleep);
                }
            }
        }
    }

    protected function getTimeOut($string)
    {
        if (strpos($string, ':') !== false) {
            $args = explode(':', $string);

            if (is_numeric($args[0])) {
                switch ($args[1]) {
                    case 's' : return (int) $args[0] * 1000000; // 1 s
                    case 'm' : return (int) $args[0] * (1000000 * 60);
                    default : return (int) $args[0];
                }
            }
        }

        return $string;
    }

    private function beforeLoad(InputInterface $input, OutputInterface $output)
    {
        if (!is_null($input->getOption('memory')) && is_numeric($input->getOption('memory'))) {
            ini_set('memory_limit', $input->getOption('memory') . 'M');
        }

        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);

        $this->sleep = $input->getOption('timeout');
    }

    final function execute(InputInterface $input, OutputInterface $output)
    {
        $this->beforeLoad($input, $output);

        try {
            $this->sleep();

            return $this->executeResult();
        } catch (\Exception $e) {
            $this->getIO()->error($e->getMessage());
            $code = ($e->getCode() === 0) ? 1 : $e->getCode();
            exit($code);
        }
    }
}