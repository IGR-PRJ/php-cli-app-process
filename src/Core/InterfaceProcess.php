<?php
namespace Core;

use Symfony\Component\Process\Process;

interface InterfaceProcess
{
    public function buildProcessCommand();

    public function finishedProcess(Process $process, $status);
}