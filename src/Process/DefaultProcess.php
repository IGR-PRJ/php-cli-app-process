<?php  declare(ticks = 1);
namespace Process;

use Controller\DefaultController;
use Core\Process;
use Symfony\Component\Process\Process as SymfonyProcess;

class DefaultProcess extends Process
{
    public function buildProcessCommand()
    {
        $this->setProcessWhile();

        $this->addProcess(DefaultController::class, 'test', [], [
            'timeout' => '1:s',
        ], 3);

        $this->addProcess(DefaultController::class, 'testParameters', [
            'string' => 'test string',
            'array' => [],
            'optionArr' => [1,2]
        ], [
            'timeout' => '1:s',
        ]);

        $this->addProcessCommand('test_os_command', 'pwd', [
            'reload' => true,
            'reloadInterval' => true,
            'interval' => '1:s',
            'timeout' => '1:s',
        ]);
    }

    public function finishedProcess(SymfonyProcess $process, $status)
    {
        if ($status === Process::STATUS_ERROR) {
            $this->getIO()->error($process->getOutput());
        } else {
            $this->getOutput()->writeln($process->getOutput() . ' - ' . $status);
        }
    }
}