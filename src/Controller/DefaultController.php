<?php declare(ticks = 1);
namespace Controller;

use Core\Controller;

class DefaultController extends Controller
{
    public function testAction()
    {
        $this->getOutput()->writeln('hello world');
    }

    public function testParametersAction($string, array $array, $option = null, array $optionArr = [])
    {
        $this->getOutput()->writeln(json_encode([
            'string' => $string,
            'array' => $array,
            'option' => $option,
        ], true));
    }
}